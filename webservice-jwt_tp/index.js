const config = require("config");
const mongoose = require("mongoose");
const AccountsRoute = require("./routes/accounts.route");
const RetraitsRoute = require("./routes/retraits.route");
const VirementsRoute = require("./routes/virements.route");
const session = require('express-session');
const express = require("express");
const app = express();


//use config module to get the privatekey, if no private key set, end the application
if (!config.get("cle")) {
  console.error("FATAL ERROR: la clé secrète n'est pas définie.");
  process.exit(1);
}

const uri = "mongodb+srv://lorenzo:cZT6SOG7f14PEPhn@clusterjwt-oa1ky.gcp.mongodb.net/test?retryWrites=true&w=majority"

mongoose.connect(uri, { useNewUrlParser: true })
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('connected to database'))


app.use(express.json());
app.use(session({ secret: 'session_secret' }));

//use users route for api/users
app.use("/api/accounts", AccountsRoute);
app.use("/api/retraits", RetraitsRoute);
app.use("/api/virements", VirementsRoute);

const port = 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));