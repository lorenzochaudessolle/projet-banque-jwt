const auth = require("../middleware/auth").authentification;
const bcrypt = require("bcrypt");
const { Account, validate } = require("../models/account.model");
const express = require("express");
const router = express.Router();

/* FONCTIONNEL */
router.get("/:idCompte", auth, async (req, res) => {
  const account = await Account.findOne({ idCompte: req.params.idCompte });
  res.send(account);
});

/* FONCTIONNEL */
router.post("/", async (req, res) => {
  // validate the request body first
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  //find an existing user
  let account = await Account.findOne({ idCompte: req.body.idCompte });
  if (account) return res.status(400).send("Compte déjà créé.");

  account = new Account({
    idCompte: req.body.idCompte,
    pinCode: req.body.pinCode,
    nameUser: req.body.nameUser,
    firstnameUser: req.body.nameUser,
    civilite: req.body.civilite,
    dateNaissance: req.body.dateNaissance,
    adresse: req.body.adresse,
    solde: req.body.solde
  });

  await account.save();

  res.status(200).send({
    message: 'Compte créé',
  });
});

/* FONCTIONNEL */
router.post("/login", async (req, res) => {

  let account = await Account.findOne({ idCompte: req.body.idCompte });
  if (!account)
    res.status(400).send("Le compte n'existe pas");

  if (account.pinCode != req.body.pinCode) {
    res.status(400).send("Le mot de passe est incorrect");
  }
  const token = account.generateAuthToken();

  if (req.session.accessToken) {
    res.status(200).send({
      message: 'déja connecté',
      accesToken: req.session.accessToken
    });
  }
  else {
    req.session.accessToken = token

    res.status(200).send({
      message: 'Connection réussie',
      token: token
    });
  }

});

/* FONCTIONNEL */
router.post("/logout", async (req, res) => {
  req.session.accessToken = '';
  res.status(200).send({
    message: req.session
  });
});


module.exports = router;