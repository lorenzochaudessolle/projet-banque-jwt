const auth = require("../middleware/auth").authentification;
const bcrypt = require("bcrypt");
const { Retrait, validate } = require("../models/retrait.model");
const { Account } = require("../models/account.model");
const express = require("express");
const router = express.Router();

/* FONCTIONNEL SANS TOKEN */
router.get("/:idRetrait", async (req, res) => {
  const retrait = await Retrait.findById(req.params.idRetrait);
  res.send(retrait);
});

/* FONCTIONNEL SANS TOKEN */

router.post("/", async (req, res) => {
  // validate the request body first
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  retrait = new Retrait({
    idCompte: req.body.idCompte,
    montant: req.body.montant,
    dateRetrait: req.body.dateRetrait
  });
  await retrait.save();
  //user.password = await bcrypt.hash(user.password, 10);
  let account = await Account.findOne({ idCompte: req.body.idCompte });
  account.solde += req.body.montant
  await account.save();
  
  res.status(200).send({
    message: retrait
  });
});


module.exports = router;