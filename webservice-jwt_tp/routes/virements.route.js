const auth = require("../middleware/auth").authentification;
const bcrypt = require("bcrypt");
const { Virement, validate } = require("../models/virement.model");
const express = require("express");
const router = express.Router();

/* PAS TERMINE */
router.get("/:idVirement", auth, async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  res.send(user);
});

/* PAS TERMINE */
router.post("/", async (req, res) => {
  // validate the request body first
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);


  virement = new Virement({
    idCompteSource: req.body.idCompteSource,
    idCompteDest: req.body.idCompteDest,
    montant: req.body.montant,
    dateVirement: req.body.dateVirement
  });
  //user.password = await bcrypt.hash(user.password, 10);
  await virement.save();

  const tokenGeneration = virement.generateAuthToken();
  var token = tokenGeneration.token

  res.cookie('accessToken', token, { secure: true, httpOnly: true }).send({
    message: 'Enjoy your token!',
    xsrfToken: tokenGeneration.xsrfToken
  });
});

module.exports = router;