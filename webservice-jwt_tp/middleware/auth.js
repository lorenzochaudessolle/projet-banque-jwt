const jwt = require("jsonwebtoken");
const config = require("config");

module.exports = {

  /* FONCTIONNEL */
  authentification: function (req, res, next) {

    var token = req.session.accessToken

    if (!token) return res.status(401).send("Acces refusé, pas de token");

    try {
      const decoded = jwt.verify(token, config.get("cle"));
      req.account = decoded;
      next();

    } catch (ex) {
      //si Le token est invaLide
      return res.status(403).send("Token invalide");
    }
  }

}