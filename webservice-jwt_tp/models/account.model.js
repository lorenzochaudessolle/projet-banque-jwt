const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');
const uid = require('rand-token').uid;

//schema du User
const AccountSchema = new mongoose.Schema({
  idCompte: {
    type: String,
    required: true,
  },
  pinCode: {
    type: Number,
    required: true,
  },
  nameUser: {
    type: String,
    required: true
  },
  firstnameUser: {
    type: String,
    required: true
  },
  dateNaissance: {
    type: Date,
    required: true
  },
  civilite: {
    type: Boolean,
    required: true
  },
  adresse: {
    type: String,
    required: true
  },
  solde: {
    type: Number,
    required: true
  }

});


//custom method to generate authToken 
AccountSchema.methods.generateAuthToken = function () {
  const token = jwt.sign({ idCompte: this.idCompte}, config.get('cle'), { expiresIn: 1500 }); //config.get permet de recuperer le champs 'myprivatekey'
  return token;
}

const Account = mongoose.model('Account', AccountSchema);

//function to validate user 
function validateAccount(account) {
  const schema = {
    idCompte: Joi.string().required(),
    pinCode: Joi.number().required(),
    nameUser: Joi.string().required(),
    firstnameUser: Joi.required(),
    dateNaissance: Joi.date().required(),
    civilite: Joi.boolean().required(),
    adresse: Joi.required(),
    solde: Joi.number().min(0).required()
  };

  return Joi.validate(account, schema);
}

exports.Account = Account;
exports.validate = validateAccount;