const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');
const uid = require('rand-token').uid;

//schema du User
const RetraitSchema = new mongoose.Schema({
  idCompte: {
    type: String,
    required: true,
  },
  dateRetrait: {
    type: Date,
    default: Date.now(),
    required: true
  },
  montant: {
    type: Number,
    required: true
  }

});


//custom method to generate authToken 
RetraitSchema.methods.generateAuthToken = function () {
  let xsrfToken = uid(16); //génère une cLé aLéatoire
  const token = jwt.sign({ _id: this._id, isAdmin: this.isAdmin, xsrfToken: xsrfToken }, config.get('cle'), { expiresIn: 120 }); //config.get permet de recuperer le champs 'myprivatekey'
  return { token, xsrfToken };
}

const Retrait = mongoose.model('Retrait', RetraitSchema);

//function to validate user 
function validateRetrait(Retrait) {
  const schema = {
    idCompte: Joi.string().required(),
    montant: Joi.number().required()
  };

  return Joi.validate(Retrait, schema);
}

exports.Retrait = Retrait;
exports.validate = validateRetrait;