const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');
const uid = require('rand-token').uid;

//schema du User
const VirementSchema = new mongoose.Schema({
  idCompteSource: {
    type: String,
    required: true,
  },
  idCompteDest: {
    type: String,
    required: true,
  },
  montant: {
    type: Number,
    required: true
  },
  dateVirement: {
    type: Date,
    default: Date.now(),
    required: true
  },

});


//custom method to generate authToken 
VirementSchema.methods.generateAuthToken = function () {
  let xsrfToken = uid(16); //génère une cLé aLéatoire
  const token = jwt.sign({ _id: this._id, isAdmin: this.isAdmin, xsrfToken: xsrfToken }, config.get('cle'), { expiresIn: 120 }); //config.get permet de recuperer le champs 'myprivatekey'
  return { token, xsrfToken };
}

const Virement = mongoose.model('Virement', VirementSchema);

//function to validate user 
function validateVirement(Virement) {
  const schema = {
    idCompteSource: Joi.string().required(),
    idCompteDest: Joi.string().required(),
    montant: Joi.number().required()
  };

  return Joi.validate(Virement, schema);
}

exports.Virement = Virement;
exports.validate = validateVirement;